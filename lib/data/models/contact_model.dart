class Contact {
  String _id;
  String _name;
  String _email;
  String _contactNo;
  String _message;

  Contact(
      {String id,
      String name,
      String email,
      String contactNo,
      String message}) {
    this._id = id;
    this._name = name;
    this._email = email;
    this._contactNo = contactNo;
    this._message = message;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get email => _email;
  set email(String email) => _email = email;
  String get contactNo => _contactNo;
  set contactNo(String contactNo) => _contactNo = contactNo;
  String get message => _message;
  set message(String message) => _message = message;

  Contact.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _email = json['email'];
    _contactNo = json['contact_no'];
    _message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['email'] = this._email;
    data['contact_no'] = this._contactNo;
    data['message'] = this._message;
    return data;
  }
}
