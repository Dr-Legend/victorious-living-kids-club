class Motivation {
  List<DailyMotivations> _dailyMotivations;

  Motivation({List<DailyMotivations> dailyMotivations}) {
    this._dailyMotivations = dailyMotivations;
  }

  List<DailyMotivations> get dailyMotivations => _dailyMotivations;
  set dailyMotivations(List<DailyMotivations> dailyMotivations) =>
      _dailyMotivations = dailyMotivations;

  Motivation.fromJson(Map<String, dynamic> json) {
    if (json['dailyMotivations'] != null) {
      _dailyMotivations = new List<DailyMotivations>();
      json['dailyMotivations'].forEach((v) {
        _dailyMotivations.add(new DailyMotivations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._dailyMotivations != null) {
      data['dailyMotivations'] =
          this._dailyMotivations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DailyMotivations {
  Image _image;

  DailyMotivations({Image image}) {
    this._image = image;
  }

  Image get image => _image;
  set image(Image image) => _image = image;

  DailyMotivations.fromJson(Map<String, dynamic> json) {
    _image = json['image'] != null ? new Image.fromJson(json['image']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._image != null) {
      data['image'] = this._image.toJson();
    }
    return data;
  }
}

class Image {
  String _hash;
  String _ext;

  Image({String hash, String ext}) {
    this._hash = hash;
    this._ext = ext;
  }

  String get hash => _hash;
  set hash(String hash) => _hash = hash;
  String get ext => _ext;
  set ext(String ext) => _ext = ext;

  Image.fromJson(Map<String, dynamic> json) {
    _hash = json['hash'];
    _ext = json['ext'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['hash'] = this._hash;
    data['ext'] = this._ext;
    return data;
  }
}
