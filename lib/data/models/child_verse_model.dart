class VerseChild {
  List<ChildVerses> _childVerses;

  VerseChild({List<ChildVerses> childVerses}) {
    this._childVerses = childVerses;
  }

  List<ChildVerses> get childVerses => _childVerses;
  set childVerses(List<ChildVerses> childVerses) => _childVerses = childVerses;

  VerseChild.fromJson(Map<String, dynamic> json) {
    if (json['childVerses'] != null) {
      _childVerses = new List<ChildVerses>();
      json['childVerses'].forEach((v) {
        _childVerses.add(new ChildVerses.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._childVerses != null) {
      data['childVerses'] = this._childVerses.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ChildVerses {
  String _description;

  ChildVerses({String description}) {
    this._description = description;
  }

  String get description => _description;
  set description(String description) => _description = description;

  ChildVerses.fromJson(Map<String, dynamic> json) {
    _description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this._description;
    return data;
  }
}
