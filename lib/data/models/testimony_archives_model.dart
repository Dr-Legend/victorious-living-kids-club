class TestimonyArchives {
  List<Testimonies> _testimonies;

  TestimonyArchives({List<Testimonies> testimonies}) {
    this._testimonies = testimonies;
  }

  List<Testimonies> get testimonies => _testimonies;
  set testimonies(List<Testimonies> testimonies) => _testimonies = testimonies;

  TestimonyArchives.fromJson(Map<String, dynamic> json) {
    if (json['testimonies'] != null) {
      final seen = Set<String>();
      var lists = List<Testimonies>();
      _testimonies = List<Testimonies>();

      json['testimonies'].toSet().toList().forEach((v) {
        lists.add(new Testimonies.fromJson(v));
      });
      //filter unique values
      final unique = lists.where((str) => seen.add(str._substrDate)).toList();

      _testimonies = unique;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._testimonies != null) {
      data['testimonies'] = this._testimonies.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Testimonies {
  String _substrDate;

  Testimonies({String substrDate}) {
    this._substrDate = substrDate;
  }

  String get substrDate => _substrDate;
  set substrDate(String substrDate) => _substrDate = substrDate;

  Testimonies.fromJson(Map<String, dynamic> json) {
    _substrDate = json['substr_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['substr_date'] = this._substrDate;
    return data;
  }
}
