class HomeImage {
  List<HomeImages> _homeImages;

  HomeImage({List<HomeImages> homeImages}) {
    this._homeImages = homeImages;
  }

  List<HomeImages> get homeImages => _homeImages;
  set homeImages(List<HomeImages> homeImages) => _homeImages = homeImages;

  HomeImage.fromJson(Map<String, dynamic> json) {
    if (json['homeImages'] != null) {
      _homeImages = new List<HomeImages>();
      json['homeImages'].forEach((v) {
        _homeImages.add(new HomeImages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._homeImages != null) {
      data['homeImages'] = this._homeImages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HomeImages {
  Image _image;

  HomeImages({Image image}) {
    this._image = image;
  }

  Image get image => _image;
  set image(Image image) => _image = image;

  HomeImages.fromJson(Map<String, dynamic> json) {
    _image = json['image'] != null ? new Image.fromJson(json['image']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._image != null) {
      data['image'] = this._image.toJson();
    }
    return data;
  }
}

class Image {
  String _url;

  Image({String url}) {
    this._url = url;
  }

  String get url => _url;
  set url(String url) => _url = url;

  Image.fromJson(Map<String, dynamic> json) {
    _url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = this._url;
    return data;
  }
}
