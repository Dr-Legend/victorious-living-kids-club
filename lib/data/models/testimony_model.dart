class Testimony {
  List<Testimonies> _testimonies;

  Testimony({List<Testimonies> testimonies}) {
    this._testimonies = testimonies;
  }

  List<Testimonies> get testimonies => _testimonies;
  set testimonies(List<Testimonies> testimonies) => _testimonies = testimonies;

  Testimony.fromJson(Map<String, dynamic> json) {
    if (json['testimonies'] != null) {
      _testimonies = new List<Testimonies>();
      json['testimonies'].forEach((v) {
        _testimonies.add(new Testimonies.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._testimonies != null) {
      data['testimonies'] = this._testimonies.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Testimonies {
  String _title;
  String _date;
  Image _image;
  String _description;
  String _substrDate;

  Testimonies(
      {String title,
      String date,
      Image image,
      String description,
      String substrDate}) {
    this._title = title;
    this._date = date;
    this._image = image;
    this._description = description;
    this._substrDate = substrDate;
  }

  String get title => _title;
  set title(String title) => _title = title;
  String get date => _date;
  set date(String date) => _date = date;
  Image get image => _image;
  set image(Image image) => _image = image;
  String get description => _description;
  set description(String description) => _description = description;
  String get substrDate => _substrDate;
  set substrDate(String substrDate) => _substrDate = substrDate;

  Testimonies.fromJson(Map<String, dynamic> json) {
    _title = json['title'];
    _date = json['date'];
    _image = json['image'] != null ? new Image.fromJson(json['image']) : null;
    _description = json['description'];
    _substrDate = json['substr_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this._title;
    data['date'] = this._date;
    if (this._image != null) {
      data['image'] = this._image.toJson();
    }
    data['description'] = this._description;
    data['substr_date'] = this._substrDate;
    return data;
  }
}

class Image {
  String _hash;
  String _ext;

  Image({String hash, String ext}) {
    this._hash = hash;
    this._ext = ext;
  }

  String get hash => _hash;
  set hash(String hash) => _hash = hash;
  String get ext => _ext;
  set ext(String ext) => _ext = ext;

  Image.fromJson(Map<String, dynamic> json) {
    _hash = json['hash'];
    _ext = json['ext'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['hash'] = this._hash;
    data['ext'] = this._ext;
    return data;
  }
}
