import 'dart:io' show File;
import 'package:http/http.dart';

class RegisterAChild {
  String _name;
  String _dob;
  String _photo;
  String _parentName;
  String _postalAddress;
  String _email;
  String _mobile;
  String _whatsapp;
  String _realationWithChild;
  String _message;

  RegisterAChild(
      {String name,
      String dob,
      String photo,
      String parentName,
      String postalAddress,
      String email,
      String mobile,
      String whatsapp,
      String realationWithChild,
      String message}) {
    this._name = name;
    this._dob = dob;
    this._photo = photo;
    this._parentName = parentName;
    this._postalAddress = postalAddress;
    this._email = email;
    this._mobile = mobile;
    this._whatsapp = whatsapp;
    this._realationWithChild = realationWithChild;
    this._message = message;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get dob => _dob;
  set dob(String dob) => _dob = dob;
  String get photo => _photo;
  set photo(String photo) => _photo = photo;
  String get parentName => _parentName;
  set parentName(String parentName) => _parentName = parentName;
  String get postalAddress => _postalAddress;
  set postalAddress(String postalAddress) => _postalAddress = postalAddress;
  String get email => _email;
  set email(String email) => _email = email;
  String get mobile => _mobile;
  set mobile(String mobile) => _mobile = mobile;
  String get whatsapp => _whatsapp;
  set whatsapp(String whatsapp) => _whatsapp = whatsapp;
  String get realationWithChild => _realationWithChild;
  set realationWithChild(String realationWithChild) =>
      _realationWithChild = realationWithChild;
  String get message => _message;
  set message(String message) => _message = message;

  RegisterAChild.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _dob = json['dob'];
    _photo = json['photo'];
    _parentName = json['parent_name'];
    _postalAddress = json['postal_address'];
    _email = json['email'];
    _mobile = json['mobile'];
    _whatsapp = json['whatsapp'];
    _realationWithChild = json['realation_with_child'];
    _message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['dob'] = this._dob;
    data['photo'] = this.photo;
    data['parent_name'] = this._parentName;
    data['postal_address'] = this._postalAddress;
    data['email'] = this._email;
    data['mobile'] = this._mobile;
    data['whatsapp'] = this._whatsapp;
    data['realation_with_child'] = this._realationWithChild;
    data['message'] = this._message;
    return data;
  }
}
