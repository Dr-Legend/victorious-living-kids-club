class PrayerRequest {
  int _id;
  String _name;
  String _phone;
  String _email;
  String _city;
  String _state;
  String _country;
  String _prayerFor;
  String _message;
  String _createdAt;
  String _updatedAt;
  String _address;

  PrayerRequest(
      {int id,
      String name,
      String phone,
      String email,
      String city,
      String state,
      String country,
      String prayerFor,
      String message,
      String createdAt,
      String updatedAt,
      String address}) {
    this._id = id;
    this._name = name;
    this._phone = phone;
    this._email = email;
    this._city = city;
    this._state = state;
    this._country = country;
    this._prayerFor = prayerFor;
    this._message = message;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._address = address;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;
  String get email => _email;
  set email(String email) => _email = email;
  String get city => _city;
  set city(String city) => _city = city;
  String get state => _state;
  set state(String state) => _state = state;
  String get country => _country;
  set country(String country) => _country = country;
  String get prayerFor => _prayerFor;
  set prayerFor(String prayerFor) => _prayerFor = prayerFor;
  String get message => _message;
  set message(String message) => _message = message;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get address => _address;
  set address(String address) => _address = address;

  PrayerRequest.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _phone = json['phone'];
    _email = json['email'];
    _city = json['city'];
    _state = json['state'];
    _country = json['country'];
    _prayerFor = json['prayer_for'];
    _message = json['message'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _address = json['address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['phone'] = this._phone;
    data['email'] = this._email;
    data['city'] = this._city;
    data['state'] = this._state;
    data['country'] = this._country;
    data['prayer_for'] = this._prayerFor;
    data['message'] = this._message;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['address'] = this._address;
    return data;
  }
}
