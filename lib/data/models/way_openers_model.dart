class WayOpeners {
  int _id;
  String _interestedClub;
  String _oneTimeGift;
  String _monthlyPledge;
  String _name;
  String _address;
  String _zip;
  String _email;
  String _mobile;
  String _whatsapp;
  String _message;
  String _createdAt;
  String _updatedAt;
  String _interestedSpeaker;

  WayOpeners(
      {int id,
      String interestedClub,
      String oneTimeGift,
      String monthlyPledge,
      String name,
      String address,
      String zip,
      String email,
      String mobile,
      String whatsapp,
      String message,
      String createdAt,
      String updatedAt,
      String interestedSpeaker}) {
    this._id = id;
    this._interestedClub = interestedClub;
    this._oneTimeGift = oneTimeGift;
    this._monthlyPledge = monthlyPledge;
    this._name = name;
    this._address = address;
    this._zip = zip;
    this._email = email;
    this._mobile = mobile;
    this._whatsapp = whatsapp;
    this._message = message;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._interestedSpeaker = interestedSpeaker;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get interestedClub => _interestedClub;
  set interestedClub(String interestedClub) => _interestedClub = interestedClub;
  String get oneTimeGift => _oneTimeGift;
  set oneTimeGift(String oneTimeGift) => _oneTimeGift = oneTimeGift;
  String get monthlyPledge => _monthlyPledge;
  set monthlyPledge(String monthlyPledge) => _monthlyPledge = monthlyPledge;
  String get name => _name;
  set name(String name) => _name = name;
  String get address => _address;
  set address(String address) => _address = address;
  String get zip => _zip;
  set zip(String zip) => _zip = zip;
  String get email => _email;
  set email(String email) => _email = email;
  String get mobile => _mobile;
  set mobile(String mobile) => _mobile = mobile;
  String get whatsapp => _whatsapp;
  set whatsapp(String whatsapp) => _whatsapp = whatsapp;
  String get message => _message;
  set message(String message) => _message = message;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get interestedSpeaker => _interestedSpeaker;
  set interestedSpeaker(String interestedSpeaker) =>
      _interestedSpeaker = interestedSpeaker;

  WayOpeners.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _interestedClub = json['interested_club'];
    _oneTimeGift = json['one_time_gift'];
    _monthlyPledge = json['monthly_pledge'];
    _name = json['name'];
    _address = json['address'];
    _zip = json['zip'];
    _email = json['email'];
    _mobile = json['mobile'];
    _whatsapp = json['whatsapp'];
    _message = json['message'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _interestedSpeaker = json['interested_speaker'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['interested_club'] = this._interestedClub;
    data['one_time_gift'] = this._oneTimeGift;
    data['monthly_pledge'] = this._monthlyPledge;
    data['name'] = this._name;
    data['address'] = this._address;
    data['zip'] = this._zip;
    data['email'] = this._email;
    data['mobile'] = this._mobile;
    data['whatsapp'] = this._whatsapp;
    data['message'] = this._message;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['interested_speaker'] = this._interestedSpeaker;
    return data;
  }
}
