class VideoArchiveDates {
  List<Videos> _videos;

  VideoArchiveDates({List<Videos> videos}) {
    this._videos = videos;
  }

  List<Videos> get videos => _videos;
  set videos(List<Videos> videos) => _videos = videos;

  VideoArchiveDates.fromJson(Map<String, dynamic> json) {
    if (json['videos'] != null) {
      final seen = Set<String>();
      var lists = List<Videos>();
      _videos = List<Videos>();

      json['videos'].toSet().toList().forEach((v) {
        lists.add(new Videos.fromJson(v));
      });
      //filter unique values
      final unique = lists.where((str) => seen.add(str._substrDate)).toList();

      _videos = unique;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._videos != null) {
      data['videos'] = this._videos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Videos {
  String _substrDate;

  Videos({String substrDate}) {
    this._substrDate = substrDate;
  }

  String get substrDate => _substrDate;
  set substrDate(String substrDate) => _substrDate = substrDate;

  Videos.fromJson(Map<String, dynamic> json) {
    _substrDate = json['substr_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['substr_date'] = this._substrDate;
    return data;
  }
}
