class Video {
  List<Videos> _videos;

  Video({List<Videos> videos}) {
    this._videos = videos;
  }

  List<Videos> get videos => _videos;
  set videos(List<Videos> videos) => _videos = videos;

  Video.fromJson(Map<String, dynamic> json) {
    if (json['videos'] != null) {
      _videos = new List<Videos>();
      json['videos'].forEach((v) {
        _videos.add(new Videos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._videos != null) {
      data['videos'] = this._videos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Videos {
  String _title;
  String _youtubeVideoId;

  Videos({String title, String youtubeVideoId}) {
    this._title = title;
    this._youtubeVideoId = youtubeVideoId;
  }

  String get title => _title;
  set title(String title) => _title = title;
  String get youtubeVideoId => _youtubeVideoId;
  set youtubeVideoId(String youtubeVideoId) => _youtubeVideoId = youtubeVideoId;

  Videos.fromJson(Map<String, dynamic> json) {
    _title = json['title'];
    _youtubeVideoId = json['youtube_video_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this._title;
    data['youtube_video_id'] = this._youtubeVideoId;
    return data;
  }
}
