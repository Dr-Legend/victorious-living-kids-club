class ChildPrayer {
  List<ChildPrayers> _childPrayers;

  ChildPrayer({List<ChildPrayers> childPrayers}) {
    this._childPrayers = childPrayers;
  }

  List<ChildPrayers> get childPrayers => _childPrayers;
  set childPrayers(List<ChildPrayers> childPrayers) =>
      _childPrayers = childPrayers;

  ChildPrayer.fromJson(Map<String, dynamic> json) {
    if (json['childPrayers'] != null) {
      _childPrayers = new List<ChildPrayers>();
      json['childPrayers'].forEach((v) {
        _childPrayers.add(new ChildPrayers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._childPrayers != null) {
      data['childPrayers'] = this._childPrayers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ChildPrayers {
  String _id;
  String _title;
  AudioFIle _audioFIle;
  String _description;

  ChildPrayers(
      {String id, String title, AudioFIle audioFIle, String description}) {
    this._id = id;
    this._title = title;
    this._audioFIle = audioFIle;
    this._description = description;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get title => _title;
  set title(String title) => _title = title;
  AudioFIle get audioFIle => _audioFIle;
  set audioFIle(AudioFIle audioFIle) => _audioFIle = audioFIle;
  String get description => _description;
  set description(String description) => _description = description;

  ChildPrayers.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _audioFIle = json['audio_fIle'] != null
        ? new AudioFIle.fromJson(json['audio_fIle'])
        : null;
    _description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    if (this._audioFIle != null) {
      data['audio_fIle'] = this._audioFIle.toJson();
    }
    data['description'] = this._description;
    return data;
  }
}

class AudioFIle {
  String _url;

  AudioFIle({String url}) {
    this._url = url;
  }

  String get url => _url;
  set url(String url) => _url = url;

  AudioFIle.fromJson(Map<String, dynamic> json) {
    _url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = this._url;
    return data;
  }
}
