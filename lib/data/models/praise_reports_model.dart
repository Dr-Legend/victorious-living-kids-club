class PraiseReportData {
  int _id;
  String _name;
  String _address;
  String _zip;
  String _email;
  String _mobile;
  String _testimony;
  String _createdAt;
  String _updatedAt;

  PraiseReportData(
      {int id,
      String name,
      String address,
      String zip,
      String email,
      String mobile,
      String testimony,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._name = name;
    this._address = address;
    this._zip = zip;
    this._email = email;
    this._mobile = mobile;
    this._testimony = testimony;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get address => _address;
  set address(String address) => _address = address;
  String get zip => _zip;
  set zip(String zip) => _zip = zip;
  String get email => _email;
  set email(String email) => _email = email;
  String get mobile => _mobile;
  set mobile(String mobile) => _mobile = mobile;
  String get testimony => _testimony;
  set testimony(String testimony) => _testimony = testimony;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  PraiseReportData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _address = json['address'];
    _zip = json['zip'];
    _email = json['email'];
    _mobile = json['mobile'];
    _testimony = json['testimony'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['address'] = this._address;
    data['zip'] = this._zip;
    data['email'] = this._email;
    data['mobile'] = this._mobile;
    data['testimony'] = this._testimony;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
