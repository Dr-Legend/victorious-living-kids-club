import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';

class FormBuilderLiteRollingSwitch extends StatefulWidget {
  final String attribute;
  final List<FormFieldValidator> validators;
  final bool initialValue;
  final bool readOnly;
  final InputDecoration decoration;
  final ValueChanged onChanged;
  final ValueTransformer valueTransformer;
  final String textOn;
  final String textOff;
  final double textSize;
  final Widget label;

  /// The color to use when this switch is on.
  ///
  /// Defaults to [ThemeData.toggleableActiveColor].
  final Color activeColor;

  /// The color to use on the track when this switch is on.
  ///
  /// Defaults to [ThemeData.toggleableActiveColor] with the opacity set at 50%.
  ///
  /// Ignored if this switch is created with [Switch.adaptive].
  final Color activeTrackColor;

  /// The color to use on the thumb when this switch is off.
  ///
  /// Defaults to the colors described in the Material design specification.
  ///
  /// Ignored if this switch is created with [Switch.adaptive].
  final Color inactiveThumbColor;

  /// The color to use on the track when this switch is off.
  ///
  /// Defaults to the colors described in the Material design specification.
  ///
  /// Ignored if this switch is created with [Switch.adaptive].
  final Color inactiveTrackColor;

  /// An image to use on the thumb of this switch when the switch is on.
  ///
  /// Ignored if this switch is created with [Switch.adaptive].
  final ImageProvider activeThumbImage;

  /// An image to use on the thumb of this switch when the switch is off.
  ///
  /// Ignored if this switch is created with [Switch.adaptive].
  final ImageProvider inactiveThumbImage;

  /// Configures the minimum size of the tap target.
  ///
  /// Defaults to [ThemeData.materialTapTargetSize].
  ///
  /// See also:
  ///
  ///  * [MaterialTapTargetSize], for a description of how this affects tap targets.
  final MaterialTapTargetSize materialTapTargetSize;

  /// {@macro flutter.cupertino.switch.dragStartBehavior}
  final DragStartBehavior dragStartBehavior;
  final FormFieldSetter onSaved;
  final EdgeInsets contentPadding;

  FormBuilderLiteRollingSwitch(
      {Key key,
      @required this.attribute,
      @required this.label,
      this.initialValue,
      this.validators = const [],
      this.readOnly = false,
      this.decoration = const InputDecoration(),
      this.onChanged,
      this.valueTransformer,
      this.activeColor,
      this.activeTrackColor,
      this.inactiveThumbColor,
      this.inactiveTrackColor,
      this.activeThumbImage,
      this.inactiveThumbImage,
      this.materialTapTargetSize,
      this.dragStartBehavior = DragStartBehavior.start,
      this.onSaved,
      this.contentPadding = const EdgeInsets.all(0.0),
      this.textOff = "Off",
      this.textOn = "On",
      this.textSize = 16.0})
      : super(key: key);

  @override
  _FormBuilderLiteRollingSwitchState createState() =>
      _FormBuilderLiteRollingSwitchState();
}

class _FormBuilderLiteRollingSwitchState
    extends State<FormBuilderLiteRollingSwitch> {
  bool _readOnly = false;
  final GlobalKey<FormFieldState> _fieldKey = GlobalKey<FormFieldState>();
  FormBuilderState _formState;
  bool _initialValue;

  @override
  void initState() {
    _formState = FormBuilder.of(context);
    _formState?.registerFieldKey(widget.attribute, _fieldKey);
    _initialValue = widget.initialValue ??
        (_formState.initialValue.containsKey(widget.attribute)
            ? _formState.initialValue[widget.attribute]
            : null);
    super.initState();
  }

  @override
  void dispose() {
    _formState?.unregisterFieldKey(widget.attribute);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _readOnly = (_formState?.readOnly == true) ? true : widget.readOnly;

    return FormField(
        key: _fieldKey,
        enabled: !_readOnly,
        initialValue: _initialValue ?? false,
        validator: (val) {
          for (int i = 0; i < widget.validators.length; i++) {
            if (widget.validators[i](val) != null) {
              return widget.validators[i](val);
            }
          }
          return null;
        },
        onSaved: (val) {
          var transformed;
          if (widget.valueTransformer != null) {
            transformed = widget.valueTransformer(val);
            _formState?.setAttributeValue(widget.attribute, transformed);
          } else {
            _formState?.setAttributeValue(widget.attribute, val);
          }
          if (widget.onSaved != null) {
            widget.onSaved(transformed ?? val);
          }
        },
        builder: (FormFieldState<dynamic> field) {
          return InputDecorator(
            decoration: widget.decoration.copyWith(
              enabled: !_readOnly,
              errorText: field.errorText,
            ),
            child: ListTile(
              dense: true,
              isThreeLine: false,
              contentPadding: widget.contentPadding,
              title: widget.label,
              trailing: Transform.scale(
                scale: 0.6,
                child: LiteRollingSwitch(
                  textOff: this.widget.textOff,
                  textOn: this.widget.textOn,
                  textSize: this.widget.textSize,
                  value: field.value,
                  onChanged: _readOnly
                      ? null
                      : (bool value) {
                          field.setValue(value);
                          FocusScope.of(context).requestFocus(FocusNode());
                          if (widget.onChanged != null) widget.onChanged(value);
                        },
                ),
              ),
            ),
          );
        });
  }
}
