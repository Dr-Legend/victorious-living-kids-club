class Queries {
  static const getHomeImages = """
query{
  homeImages{
    image{
      url
    }
  }
}""";

  static const registerChild = """
mutation CreateRegisterChild(\$name:String,\$email:String!,\$dob:Date!,\$photo:ID!,\$parent_name:String!,\$postal_address:String!,\$mobile:String!,\$whatsapp:String!,\$realation_with_child:String!,\$message:String!){
  createRegisterChild(input:{data:{name:\$name,email:\$email,dob:\$dob,photo:\$photo,parent_name:\$parent_name,postal_address:\$postal_address,mobile:\$mobile,whatsapp:\$whatsapp,realation_with_child:\$realation_with_child,message:\$message}}){
    registerChild{
      name,
      email,id,photo{
      url
      }
    }
  }
}""";
  static const registerWayOpener = """
mutation CreateWayOpener(\$name:String,\$email:String!,\$zip:String!,\$interested_club:String!,\$monthly_pledge:String,\$address:String!,\$mobile:String!,\$whatsapp:String!,\$interested_speaker:String!,\$message:String!,\$one_time_gift:String){
  createWayOpener(input:{data:{name:\$name,email:\$email,zip:\$zip,interested_club:\$interested_club,monthly_pledge:\$monthly_pledge,address:\$address,mobile:\$mobile,whatsapp:\$whatsapp,interested_speaker:\$interested_speaker,message:\$message,one_time_gift:\$one_time_gift}}){
    wayOpener{
      name,
      email,id
    }
  }
}""";
  static const getTestimonies = """
query Testimonies(\$substr_date:String!){
  testimonies( sort:"Date:DESC",where: { substr_date: \$substr_date }) {
    title,
    date,
    image{
     hash,
      ext
    }
    description,
    substr_date
  }
}
""";
  static const getTestimoniesArchives = """
query {
  testimonies(sort:"date:DESC"){
    substr_date
  }
}""";

  static const registerContact = """
mutation Createcontact(\$name:String,\$email:String!,\$contact_no:String!,\$message:String!){
  createContact(input:{data:{name:\$name,email:\$email,contact_no:\$contact_no,message:\$message}}){
    contact{
      name,
      email,id
    }
  }
}""";
  static const registerPraiseReport = """
mutation CreatePraiseReport(\$name:String,\$email:String!,\$mobile:String!,\$testimony:String!,\$address:String!,\$zip:String!){
  createPraiseReport(input:{data:{name:\$name,email:\$email,mobile:\$mobile,testimony:\$testimony,address:\$address,zip:\$zip}}){
    praiseReport{
      name,
      email,id
    }
  }
}""";
  static const getChildPrayer = """
query{
  childPrayers(sort:"id:DESC"){
    id
    title,
    audio_fIle{
      url
    }
  description
  }
}
""";
  static const getChildVerse = """
query{
 childVerses(sort:"id:DESC"){
  description
}
}
""";
  static const getVideoArchives = """
query {
  videos{
    substr_date
  }
}""";
  static const getVideos = """
query Videos(\$substr_date:String!){
  videos( sort:"Date:DESC",where: { substr_date: \$substr_date }) {
   title,
  youtube_video_id
  }
}
""";
  static const getMotivation = """
query{
dailyMotivations(sort:"id:DESC"){
 image{
  hash,
  ext,
}
}
}
""";
  static const registerPrayer = """
mutation CreatePrayer(\$name:String,\$email:String!,\$phone:String!,\$city:String!,\$state:String!,\$country:String!,\$prayer_for:String!,\$message:String!,\$address:String!){
  createPrayer(input:{data:{name:\$name,email:\$email,phone:\$phone,city:\$city,state:\$state,country:\$country,prayer_for:\$prayer_for,message:\$message,address:\$address}}){
    prayer{
      name,
      email,id
    }
  }
}""";
}
