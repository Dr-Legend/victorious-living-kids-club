import 'package:flutter/material.dart';

class Constants {
  static const GraphQl_API = "http://vlkcnew.christianappdevelopers.com:3002/graphql";

//  static const assetPath =
//      "https://vlkcnew.christianappdevelopers.com/api/app/public";

  static const String add_child =
      """VICTORIOUS LIVING is an APP that helps the parents to pray and bless the children every day! We send in a PRAY-ALONG-PRAYER on your behalf for your child.

Parents can pray along, and also claim a promise for the child from the Bible.

WHY IS IT GOOD TO REGISTER A CHILD?

For every registered child through this APP, there will be a team of anointed men and women who will be praying for them every day! There is absolutely no obligation, no registration fees! We do this as a ministry and as a passion!

HOW DO WE REGISTER?

Just fill in the simple details below! That’s it! REGISTRATION IS COMPLETE!
""";
//  static const String add_child =
//      """At VICTORIOUS LIVING KIDS CLUB, our burden is to see a generation dedicated for the Lord! We do this as a ministry, and as a passion!
//
//If you register your child with us in this APP, there will be a team of anointed men and women who will be praying for your child every day, for each of them! There is absolutely no obligation, no registration fees, or no other requirements!
//
//WHY VICTORIOUS LIVING KIDS CLUB!
//We live in perilous times. Every evening, I thank God that my kids came home safely. Sadly, many parents have many things that make them upset because of their kids. 
//
//As you and I watch the news, we weep, we cry out in anger, and then we wonder, "How can we protect our kids from something like this?" We wonder if it's safe to send our children out the front door. We question whether the world is falling apart. And nothing is more terrifying than the realization that we cannot protect them.
//
//While we cannot hide our children away from the world, we can be proactive in guarding them. We can shield them in far greater ways than wrapping them in a plastic bubble or locking them in their rooms for the rest of their lives. You and I can pray. 
//
//Through VICTORIOUS LIVING KIDS CLUB, We intercede for the children, praying for wisdom, protection, peace, and strength. Author Mark Batterson wrote, "If you determine to circle your children in prayer, you will shape their destinies … your prayers will live on in their lives long after you die." In fact, this is what Jesus called us to do.
// 
//With this in mind, a group of dedicated men and women, pray for the Children, every day, for each one of them. """;

  static const String way_Opener = """
The Bible says in Proverbs 18:16, A gift opens the way and ushers the giver into the presence of the great.

Bible also says Proverbs 11:25 A generous person will prosper; whoever refreshes others will be refreshed.

We want to open to you an opportunity to help the less fortunate children with their food, health, education or well-being. Maybe on a monthly basis, or on a special occasion such as the birthday of your child, or simply as a one-time gift, regardless, every penny will go to some child and you will be informed how it was used.

FORM FOR  SOWING SEEDS!!

Fill out this form to be Door Opener with Victorious Living Kids Club!
  """;
//  static const String way_Opener = """
//The Bible says in Proverbs 18:16, A gift opens the way and ushers the giver into the presence of the great.
//Bible also says Proverbs 11:25 A generous person will prosper; whoever refreshes others will be refreshed.
//
//We want to open to you an opportunity to help the less fortunate children with their food, health, education or well-being. Maybe on a monthly basis, or on a special occasion such as the birthday of your child, or simply as a one-time gift, regardless, every penny will go to some child and you will be informed how it was used.
//  """;

  static const String ASSET_PATH = "https://vlkcnew.christianappdevelopers.com/v1/app/public";
  static const String ASSET_THUMBNAILS_PATH =
      "http://vlkcnew.christianappdevelopers.com:3002/uploads";

  static const String praise_report = """
Do you have a testimony to share about how God has blessed the child we have been praying for or other ways through this ministry? We would love to hear about your answered prayers or praise reports of what the Lord is doing, and rejoice with you! Please use this form to share with us as simply as possible.

Your praise report glorifies His name and God can use it to bless others. Thank you for being a blessing to others by sharing your praise report with us! This praise report will bless many others as they hear about it!
  """;

  static const String YOUTUBE_APP_KEY =
      "AIzaSyAVWo6yAuDIACxzpDulluZxonEqsxyHjrQ";

  static const String Prayer_TITLE = "Send Prayer Request";

  static const String REGISTER_DEVICE_API =
      'http://vlkcnew.christianappdevelopers.com:3002/fcms';
}

class ChurchAppColors {
  static Color darkColor = Color.fromRGBO(18, 32, 48, 1);
  static Color darkAccent = Colors.grey[800];
  static Color lightBlue = Colors.cyan;
  static Color golden = Color(0xFFEAC131);
//  static Color golden = Color(0xFFD4AF37);
  static Color lightYellow = Color(0xFFFBDC73);
  static Color pinkRed = Color(0xFFF55564);
  static Color lightPinkRed = Color(0xFFF8A2A4);
  static Color navyBlue = Color(0xFF5A6571);
  static Color scaffoldBackground = Color(0xFFFFFFFF);
  static Color brandBackground = Color(0xFF19222B);
  static Color brandBackgroundLight = Color(0xFF495764);
  static Color prayerBackground = Color(0xFF302E30);
}
