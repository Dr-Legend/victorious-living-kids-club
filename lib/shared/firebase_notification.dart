import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:victoriouslivingkidsclub/api/api.dart';
import 'package:victoriouslivingkidsclub/bloc/home/home_bloc.dart';
import 'package:victoriouslivingkidsclub/bloc/video/video_bloc.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/widgets/child_prayer/prayer_for_child.dart';
import 'package:victoriouslivingkidsclub/widgets/child_verse/child_verse.dart';
import 'package:victoriouslivingkidsclub/widgets/motivation/daily_motivation.dart';
import 'package:victoriouslivingkidsclub/widgets/video_messages/video_messages.dart';

class NotificationHandler {
  ApiMethods apiMethods = ApiMethods();
  FirebaseMessaging _fcm = FirebaseMessaging();
  SharedPreferences prefs;
  StreamSubscription iosSubscription;
  static final NotificationHandler _singleton =
      new NotificationHandler._internal();
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  AndroidDeviceInfo androidInfo;
  IosDeviceInfo iosInfo;
  factory NotificationHandler() {
    return _singleton;
  }
  NotificationHandler._internal();

  initializeFcmNotification(
      BuildContext context, RmxAudioPlayer rmxAudioPlayer) async {
    await initSharedPreference();
//    _listenForPermissionStatus();
//    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

//    var initializationSettingsAndroid =
//        new AndroidInitializationSettings('@mipmap/ic_launcher');
//    var initializationSettingsIOS = new IOSInitializationSettings(
//        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
//    var initializationSettings = new InitializationSettings(
//        initializationSettingsAndroid, initializationSettingsIOS);
//    flutterLocalNotificationsPlugin.initialize(initializationSettings,
//        onSelectNotification: onSelectNotification);

    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
        _saveDeviceToken();
      });

      _fcm.requestNotificationPermissions(
          IosNotificationSettings(sound: true, badge: true, alert: true));
    } else {
      _saveDeviceToken();
    }

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
//        final snackBar = SnackBar(
//            content: Text(
//                '${message['notification']['title']}! \n${message['notification']['body'] != null ? message['notification']['body'] : ''}'));
//        Constants.scaffoldKey.currentState.showSnackBar(snackBar);
      },
//      onBackgroundMessage: Theme.of(context).platform == TargetPlatform.iOS
//          ? null
//          : myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
//TODO: DO it later
        print("onResume: $message");
        switch (message['data']['screen']) {
          case 'testimony':
            BlocProvider.of<HomeBloc>(context).add(ChangeChildEvent(3));
            break;

          case 'daily-motivation':
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => DailyMotivation()));

            break;
          case 'child-prayer':
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => PrayerForChild()));

            break;
          case 'video':
            BlocProvider.of<VideoBloc>(context).add(LoadVideosEvent());
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => VideoMessages()));

            break;
        }
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        switch (message['data']['screen']) {
          case 'testimony':
            BlocProvider.of<HomeBloc>(context).add(ChangeChildEvent(3));
            break;

          case 'daily-motivation':
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => DailyMotivation()));

            break;
          case 'child-prayer':
            Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => PrayerForChild(
                      rmxAudioPlayer: rmxAudioPlayer,
                    )));

            break;
          case 'child-verse':
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => ChildVerse()));

            break;
          case 'video':
            BlocProvider.of<VideoBloc>(context).add(LoadVideosEvent());
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => VideoMessages()));

            break;
        }
      },
    );
  }

//  void iOSPermission() {
//    _fcm.requestNotificationPermissions(
//        IosNotificationSettings(sound: true, badge: true, alert: true));
//    _fcm.onIosSettingsRegistered
//        .listen((IosNotificationSettings settings) {
//      print("Settings registered: $settings");
//    });
//  }

  Future getDeviceId() async {
    try {
      androidInfo = await deviceInfo.androidInfo;
    } catch (e) {
      print(e);
    }
  }

  Future getIosDeviceId() async {
    try {
      iosInfo = await deviceInfo.iosInfo;
    } catch (e) {
      print(e);
    }
  }

  /// Get the token, save it to the database for current user
  _saveDeviceToken() async {
    String fcmToken = await _fcm.getToken();
    print("FCM_TOKEN: $fcmToken");
    prefs = await SharedPreferences.getInstance();
    var deviceId;
    var platform;
    if (Platform.isIOS) {
//      iOSPermission();
      getIosDeviceId();
      platform = "IOS";
    } else {
      getDeviceId();
      platform = "ANDROID";
    }
    String clientSecret = prefs.get('clientSecret');
    if (clientSecret == null) {
      _fcm.getToken().then((token) {
        var deviceName;
        if (Platform.isIOS) {
          deviceId = iosInfo.identifierForVendor;
        } else {
          deviceId = androidInfo.androidId;
        }
//          var url = deviceName + "&fcmid=" + token;
        print('Token $token');

        Map<String, dynamic> body = {
          "device_id": deviceId,
          "fcm_token": token,
          "platform": platform
        };
        registerDevice(body);
//          _presenter.makeGetCall(Constants.REGISTRATION + url, PageName.MUSIC);
      });
    } else {
      print('ALERT!,Device is Already registred');
    }
  }

  initSharedPreference() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future registerDevice(Map<String, dynamic> body) async {
    String fcmId = body['fcm_token'];
    String deviceId = body['device_id'];
    String platform = body['platform'];
    try {
      var response = await apiMethods
          .requestInGet(Constants.REGISTER_DEVICE_API + "/" + deviceId);
      Map<String, dynamic> rows =
          response != null ? jsonDecode(response) : null;
      if (rows != null) {
        Map<String, dynamic> responseJson = rows;
        if (responseJson.length == 0) {
          print('Registring new device');
          await apiMethods.requestInPost(
              Constants.REGISTER_DEVICE_API, null, body);
        } else {
          print('Device is already registered updating token..');
          await apiMethods.requestInPut(
              Constants.REGISTER_DEVICE_API +
                  "/" +
                  responseJson['id'].toString(),
              null,
              body);
        }
      } else {
        print('Registring new device');
        await apiMethods.requestInPost(
            Constants.REGISTER_DEVICE_API, null, body);
      }

      bool operationStatus = await prefs.setString('clientSecret', fcmId);
      print('Operation status: $operationStatus');
      prefs.setString('deviceId', deviceId);
      prefs.setString('platform', platform);
    } catch (e) {
      print(e);
    }
  }
}
