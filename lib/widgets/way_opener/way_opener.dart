import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:victoriouslivingkidsclub/data/models/way_openers_model.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';
import 'package:intl/intl.dart';
import 'package:victoriouslivingkidsclub/shared/widgets/custom_form_fields/custom_switch.dart';

class WayOpener extends StatelessWidget {
  final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>(debugLabel: 'way-opener-form-builder-key');
  WayOpeners wayOpeners = WayOpeners();
  @override
  Widget build(BuildContext context) {
    return KeyboardAvoider(
      autoScroll: true,
      curve: Curves.linearToEaseOut,
      child: FormBuilder(
        key: _fbKey,
        child: Mutation(
          options: MutationOptions(
            documentNode: gql(Queries.registerWayOpener),
            onCompleted: (result) {
              if (result != null) {
                print((result as LazyCacheMap).data);
                final results =
                    (result as LazyCacheMap).data['createWayOpener'];
                if (results != null) {
                  Scaffold.of(context).hideCurrentSnackBar();
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: Row(
                    children: [
                      Icon(
                        FontAwesomeIcons.checkCircle,
                        color: Colors.green,
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        "Uploaded Successfully",
                        style: TextStyle(color: Colors.green),
                      )
                    ],
                  )));
                }
              } else {
                print("null result");
              }
            },
            onError: (error) {
              print("error $error");
              Scaffold.of(context).hideCurrentSnackBar();
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Row(
                children: [
                  Icon(
                    Icons.error_outline,
                    color: Colors.red,
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Text(
                    "An error occured, please try again later..",
                    style: TextStyle(color: Colors.red),
                  )
                ],
              )));
            },
          ),
          builder: (RunMutation runMutation, QueryResult result) {
            return Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      Constants.way_Opener,
//                      style: TextStyle(
//                          color: Theme.of(context).primaryColor,
//                          fontFamily: 'Stoke'),
                    ),
                  ),
//                  Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: Text(
//                        "Fill out this form to be Door Opener with Victorious Living Kids Club!"),
//                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      validators: [
                        FormBuilderValidators.required(
                            errorText: 'Name is required'),
                      ],
                      onSaved: (name) => wayOpeners.name = name,
                      attribute: 'name',
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Name'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      onSaved: (address) => wayOpeners.address = address,
                      attribute: 'address',
                      maxLines: 10,
                      minLines: 1,
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Address'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      onSaved: (zip) => wayOpeners.zip = zip,
                      attribute: 'zip',
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Zip'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      validators: [
//                        FormBuilderValidators.email(),
                      ],
                      onSaved: (email) {
                        print(email);
                        wayOpeners.email = email;
                      },
                      attribute: 'email',
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'E-Mail id'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      onSaved: (mobile) => wayOpeners.mobile = mobile,
                      attribute: 'mobile',
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Mobile Number'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      onSaved: (whatsapp) => wayOpeners.whatsapp = whatsapp,
                      attribute: 'whatsapp',
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'WhatsApp Number'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      onSaved: (msg) => wayOpeners.message = msg,
                      attribute: 'Message',
                      maxLines: 10,
                      minLines: 1,
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Message'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderLiteRollingSwitch(
                      textOn: "Yes",
                      textOff: "No",
                      attribute: 'interested-speaker',
                      label: Text(
                          'Interested in having you as a speaker in Church, corporate or other'),
                      onSaved: (speaker) {
                        if (speaker) {
                          wayOpeners.interestedSpeaker = "Yes";
                        } else {
                          wayOpeners.interestedSpeaker = "No";
                        }
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderLiteRollingSwitch(
                      textOn: "Yes",
                      textOff: "No",
                      attribute: 'interested-club',
                      label: Text(
                          'Interested in joining VICTORIOUS LIVING KIDS CLUB as PRAYER PARTNER, committing to pray registered in the club!'),
                      onSaved: (speaker) {
                        if (speaker) {
                          wayOpeners.interestedClub = "Yes";
                        } else {
                          wayOpeners.interestedClub = "No";
                        }
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                      ),
                    ),
                  ),
                  FormBuilderCustomField(
                    attribute: "name",
                    formField: FormField(
                      builder: (FormFieldState<dynamic> field) {
                        return InputDecorator(
                          decoration: InputDecoration(
//                            helperText:
//                                'please indicate if this is a special Day',
                            labelText: 'I Would like to make a',
                            errorText: field.errorText,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: FormBuilderChoiceChip(
                                    onChanged: (value) => field.setValue(value),
                                    spacing: 20,
                                    elevation: 5,
                                    attribute: 'onetime-monthly-chip',
                                    pressElevation: 0,
                                    alignment: WrapAlignment.center,
                                    decoration:
                                        InputDecoration.collapsed(hintText: ''),
                                    options: [
                                      FormBuilderFieldOption(
                                        child: Text("One time gift"),
                                        value: "onetime",
                                      ),
                                      FormBuilderFieldOption(
                                          child: Text("Monthly pledge"),
                                          value: "monthly"),
                                    ]),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: FormBuilderTextField(
                                  onSaved: (val) {
                                    if (_fbKey
                                            .currentState
                                            .fields['onetime-monthly-chip']
                                            .currentState
                                            .value !=
                                        null) {
                                      if (_fbKey
                                              .currentState
                                              .fields['onetime-monthly-chip']
                                              .currentState
                                              .value ==
                                          'onetime') {
                                        wayOpeners.oneTimeGift = val;
                                        wayOpeners.monthlyPledge = "0";
                                      } else {
                                        wayOpeners.monthlyPledge = val;
                                        wayOpeners.oneTimeGift = "0";
                                      }
                                    }
                                  },
                                  validators: [(val) => customValidator(val)],
                                  attribute: 'onetime-monthly-amount',
                                  decoration: InputDecoration(
                                    hintText: 'amount',
                                    errorText: field.errorText,
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  MaterialButton(
                    elevation: 5,
                    color: Theme.of(context).primaryColor,
                    textColor: Theme.of(context).colorScheme.surface,
                    onPressed: () async {
                      if (_fbKey.currentState.saveAndValidate()) {
                        runMutation(
                          wayOpeners.toJson(),
                        );
                        Scaffold.of(context).showSnackBar(SnackBar(
                            content: Row(
                          children: [
                            CircularProgressIndicator(),
                            SizedBox(
                              width: 30,
                            ),
                            Text(
                              "Processing..",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        )));
                        _fbKey.currentState.reset();
                      }
                    },
                    child: Text("SUBMIT"),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  customValidator(val) {
    if (_fbKey.currentState.fields['onetime-monthly-chip'].currentState.value !=
            null &&
        (val == '' || val == null)) {
      return "please enter amount";
    }
  }
}
