import 'package:banner_view/banner_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:victoriouslivingkidsclub/bloc/video/video_bloc.dart';
import 'package:victoriouslivingkidsclub/data/models/home_images_model.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/network_image.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';
import 'package:victoriouslivingkidsclub/widgets/child_prayer/prayer_for_child.dart';
import 'package:victoriouslivingkidsclub/widgets/child_verse/child_verse.dart';
import 'package:victoriouslivingkidsclub/widgets/motivation/daily_motivation.dart';
import 'package:victoriouslivingkidsclub/widgets/prayer/prayer.dart';
import 'package:victoriouslivingkidsclub/widgets/video_messages/video_messages.dart';

class Home extends StatelessWidget {
  final RmxAudioPlayer rmxAudioPlayer;

  const Home({Key key, this.rmxAudioPlayer}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: EdgeInsets.all(12.0),
          child: Material(
            color: Theme.of(context).colorScheme.primary,
            borderRadius: BorderRadius.circular(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: SvgPicture.asset(
                        "assets/child_prayer_old.svg",
                        fit: BoxFit.fitWidth,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => PrayerForChild(
                                      rmxAudioPlayer: rmxAudioPlayer,
                                    )));
//                        var date = dateFormat.format(DateTime.now());
//                        BlocProvider.of<BlogBloc>(context).add(LoadBlogEvent());
//                        Navigator.of(context).pushNamed(Routes.blog);
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: SvgPicture.asset(
                        "assets/verse.svg",
                        fit: BoxFit.fitWidth,
                        color: Colors.white,
                      ),
                      onPressed: () {
//                        BlocProvider.of<MusicBloc>(context)
//                            .add(LoadMusicEvent());
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => ChildVerse()));
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: SvgPicture.asset(
                        "assets/play-button.svg",
                        color: Colors.white,
                        fit: BoxFit.fitWidth,
                      ),
                      onPressed: () {
                        BlocProvider.of<VideoBloc>(context)
                            .add(LoadVideosEvent());
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => VideoMessages()));
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: SvgPicture.asset(
                        "assets/motivation_old.svg",
                        fit: BoxFit.fitWidth,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => DailyMotivation()));
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: SvgPicture.asset(
                        "assets/prayer_old.svg",
                        fit: BoxFit.fitWidth,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        //sendFcm(context);
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (_) => Prayer()));
                      }),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Query(
              options: QueryOptions(documentNode: gql(Queries.getHomeImages)),
              builder: (QueryResult result,
                  {VoidCallback refetch, FetchMore fetchMore}) {
                if (result.hasException) {
                  return Center(
                    child: Text("Unable to Load data please try again!"),
                  );
                }

                if (result.loading) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                final images = HomeImage.fromJson(result.data);
                if (images.homeImages.isEmpty) {
                  return Center(
                    child: Text("Nothing to show here.."),
                  );
                }
                return BannerView(
                  images.homeImages
                      .map((image) => PNetworkImage(
                            Constants.ASSET_PATH + image.image.url,
                          ))
                      .toList(),
                  log: false,
                  autoRolling: images.homeImages.isNotEmpty,
                  indicatorSelected: Container(
                    width: 8.0,
                    height: 8.0,
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  ),
                  intervalDuration: Duration(seconds: 10),
                );
              }),
        ),
      ],
    );
  }
}
