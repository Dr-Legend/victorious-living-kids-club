import 'dart:io';
import 'package:dio/dio.dart' as dio;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'dart:isolate';
import 'package:path/path.dart';
import 'package:async/async.dart';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:victoriouslivingkidsclub/data/models/register_child_model.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';
import 'dart:convert';
import 'package:http_parser/http_parser.dart';

class RegisterChild extends StatefulWidget {
  @override
  _RegisterChildState createState() => _RegisterChildState();
}

class _RegisterChildState extends State<RegisterChild> {
  final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>(debugLabel: 'reg-child-form-builder-key');
  bool isPhotoUploading = false;
  RegisterAChild child = RegisterAChild();
  File childPhoto;
  @override
  Widget build(BuildContext context) {
    return KeyboardAvoider(
      autoScroll: true,
      curve: Curves.linearToEaseOut,
      child: FormBuilder(
        key: _fbKey,
        child: Mutation(
          options: MutationOptions(
            documentNode: gql(Queries.registerChild),
            onCompleted: (result) {
              if (result != null) {
//                  print((result as LazyCacheMap).data['createRegisterChild']);
                final results =
                    (result as LazyCacheMap).data['createRegisterChild'];
                if (results != null) {
                  Scaffold.of(context).hideCurrentSnackBar();
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: Row(
                    children: [
                      Icon(
                        FontAwesomeIcons.checkCircle,
                        color: Colors.green,
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        "Uploaded Successfully",
                        style: TextStyle(color: Colors.green),
                      )
                    ],
                  )));
                }
              } else {
                print("null result");
              }
            },
            onError: (error) {
              Scaffold.of(context).hideCurrentSnackBar();
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Row(
                children: [
                  Icon(
                    Icons.error_outline,
                    color: Colors.red,
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Text(
                    "An error occured, please try again later..",
                    style: TextStyle(color: Colors.red),
                  )
                ],
              )));
            },
          ),
          builder: (RunMutation runMutation, QueryResult result) {
            return Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      Constants.add_child,
//                      style: TextStyle(
//                          color: Theme.of(context).primaryColor,
//                          fontFamily: 'Stoke'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8.0, bottom: 8.0),
                    child: Text("FORM FOR REGISTERING A CHILD!"),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      validators: [
                        FormBuilderValidators.required(
                            errorText: 'Name is required'),
                      ],
                      onSaved: (name) => child.name = name,
                      attribute: 'name',
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Name'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderDateTimePicker(
                      validators: [
                        FormBuilderValidators.required(
                            errorText: 'Date of birth is required'),
                      ],
                      onSaved: (dob) {
                        if (dob != null) {
                          child.dob = DateFormat("yyyy-MM-dd").format(dob);
                        }
                      },
                      inputType: InputType.date,
                      attribute: 'dob',
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Date of Birth'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderImagePicker(
                      validators: [
                        FormBuilderValidators.required(
                            errorText: 'Photo is required'),
                      ],
                      onSaved: (photo) async {
                        if (photo.isNotEmpty) {
                          setState(() {
                            childPhoto = photo[0];
                          });
                        }
                      },
                      attribute: 'Photo',
                      decoration: InputDecoration(labelText: 'Photo'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      validators: [
                        FormBuilderValidators.required(
                            errorText: 'Parent Name is required'),
                      ],
                      onSaved: (pName) => child.parentName = pName,
                      attribute: 'parent-name',
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Parent Name'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      onSaved: (address) => child.postalAddress = address,
                      attribute: 'address',
                      maxLines: 10,
                      minLines: 1,
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Poastal Address'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      validators: [
                        FormBuilderValidators.email(),
                      ],
                      onSaved: (email) {
                        print(email);
                        child.email = email;
                      },
                      attribute: 'email',
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'E-Mail id'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      onSaved: (mobile) => child.mobile = mobile,
                      attribute: 'mobile',
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Mobile Number'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      onSaved: (whatsapp) => child.whatsapp = whatsapp,
                      attribute: 'whatsapp',
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'WhatsApp Number'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      validators: [
                        FormBuilderValidators.required(
                            errorText: 'Realation with child is required'),
                      ],
                      onSaved: (relation) =>
                          child.realationWithChild = relation,
                      attribute: 'ralation',
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText: 'Relationship with Child'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilderTextField(
                      onSaved: (msg) => child.message = msg,
                      attribute: 'concern',
                      maxLines: 10,
                      minLines: 1,
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          labelText:
                              'Specific prayer concerns about the child'),
                    ),
                  ),
                  MaterialButton(
                    elevation: 5,
                    color: Theme.of(context).primaryColor,
                    textColor: Theme.of(context).colorScheme.surface,
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      if (_fbKey.currentState.saveAndValidate()) {
                        if (childPhoto != null) {
                          child.photo = await upload(childPhoto);
                        }
                        runMutation(
                          child.toJson(),
                        );
                        Scaffold.of(context).showSnackBar(SnackBar(
                            content: Row(
                          children: [
                            CircularProgressIndicator(),
                            SizedBox(
                              width: 30,
                            ),
                            Text(
                              "Processing..",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        )));
                        _fbKey.currentState.reset();
                        childPhoto = null;
                      }
                    },
                    child: Text("SUBMIT"),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Future<String> upload(File image) async {
    try {
      ///[1] CREATING INSTANCE
      var dioRequest = dio.Dio();
      dioRequest.options.baseUrl = 'http://173.212.215.241:1337/upload/';

      //[2] ADDING TOKEN
      dioRequest.options.headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };

      //[3] ADDING EXTRA INFO
      var formData = dio.FormData();

      //[4] ADD IMAGE TO UPLOAD
      var file = dio.MultipartFile.fromBytes(image.readAsBytesSync(),
          filename: basename(image.path) + extension(image.path),
          contentType: MediaType(
              "image",
              extension(image.path)
                  .replaceAll("jpg", "jpeg")
                  .replaceAll('.', '')));

      formData.files.add(MapEntry('files', file));

      //[5] SEND TO SERVER
      var response = await dioRequest.post(
        'http://vlkcnew.christianappdevelopers.com:3002/upload/',
        data: formData,
      );
      final result = response.data[0];
      print('  ${result['id']}');
      return result['id'].toString();
    } catch (err) {
      print('ERROR  $err');
    }
  }
}
