import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:photo_view/photo_view.dart';
import 'package:victoriouslivingkidsclub/data/models/motivation_model.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';

class DailyMotivation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Daily Motivation"),
      ),
      body: Query(
          options: QueryOptions(documentNode: gql(Queries.getMotivation)),
          builder: (QueryResult result,
              {VoidCallback refetch, FetchMore fetchMore}) {
            if (result.hasException) {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Unable to Load data please try again!"),
                ),
              );
            }

            if (result.loading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            var images = Motivation.fromJson(result.data);
            if (images.dailyMotivations.isEmpty) {
              return Center(
                child: Text("No data to show.."),
              );
            }
            return GridView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => Material(
                                  child: Stack(
                                    children: [
                                      PhotoView(
                                        heroAttributes: PhotoViewHeroAttributes(
                                            tag: images.dailyMotivations[index]
                                                .hashCode),
                                        imageProvider:
                                            CachedNetworkImageProvider(Constants
                                                    .ASSET_THUMBNAILS_PATH +
                                                "/small_" +
                                                images.dailyMotivations[index]
                                                    .image.hash +
                                                images.dailyMotivations[index]
                                                    .image.ext
                                                    .replaceAll(" ", "%20")),
                                      ),
                                      Positioned(
                                          left: 5,
                                          top: 25,
                                          child: IconButton(
                                              icon: Icon(
                                                Icons.arrow_back,
                                                color: Colors.white,
                                              ),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              })),
                                    ],
                                  ),
                                )));
                  },
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Hero(
                        tag: images.dailyMotivations[index].hashCode,
                        child: Material(
                          elevation: 5,
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          child: Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.bottomLeft,
                                    end: Alignment.topRight,
                                    stops: [
                                      0,
                                      1,
                                      0
                                    ],
                                    colors: [
                                      Colors.redAccent,
                                      Colors.white,
                                      Colors.red
                                    ]),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: CachedNetworkImageProvider(Constants
                                            .ASSET_THUMBNAILS_PATH +
                                        "/thumbnail_" +
                                        images.dailyMotivations[index].image
                                            .hash +
                                        images.dailyMotivations[index].image.ext
                                            .replaceAll(" ", "%20")))),
                            child: Center(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.5),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                ),
                                width: double.maxFinite,
                                height: double.maxFinite,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
//                                    Padding(
//                                      padding: const EdgeInsets.only(
//                                          left: 8.0, right: 8.0),
//                                      child: Text(
//                                        state.GalleryAlbums[index].title,
//                                        maxLines: 1,
//                                        overflow: TextOverflow.ellipsis,
//                                        style: TextStyle(
//                                          color: Colors.white,
//                                        ),
//                                      ),
//                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      )),
                );
              },
              itemCount: images.dailyMotivations.length,
            );
          }),
    );
  }
}
