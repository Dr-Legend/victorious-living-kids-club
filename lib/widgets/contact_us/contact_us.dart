import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:victoriouslivingkidsclub/data/models/contact_model.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>(debugLabel: 'contact-us-form-builder-key');

  Contact contact = Contact();

  @override
  Widget build(BuildContext context) {
    return KeyboardAvoider(
      autoScroll: true,
      curve: Curves.linearToEaseOut,
      child: FormBuilder(
        key: _fbKey,
        child: Mutation(
          options: MutationOptions(
            documentNode: gql(Queries.registerContact),
            onCompleted: (result) {
              if (result != null) {
                print((result as LazyCacheMap).data);
                final results = (result as LazyCacheMap).data['createContact'];
                if (results != null) {
                  Scaffold.of(context).hideCurrentSnackBar();
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: Row(
                    children: [
                      Icon(
                        FontAwesomeIcons.checkCircle,
                        color: Colors.green,
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        "Uploaded Successfully",
                        style: TextStyle(color: Colors.green),
                      )
                    ],
                  )));
                }
              } else {
                print("null result");
              }
            },
            onError: (error) {
              print("error $error");
              Scaffold.of(context).hideCurrentSnackBar();
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Row(
                children: [
                  Icon(
                    Icons.error_outline,
                    color: Colors.red,
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Text(
                    "An error occured, please try again later..",
                    style: TextStyle(color: Colors.red),
                  )
                ],
              )));
            },
          ),
          builder: (RunMutation runMutation, QueryResult result) {
            return Container(
//              color: Color(0xFF495764),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Image.asset('assets/logo/logo_r.png'),
                    SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Send us a Message',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FormBuilderTextField(
                        textInputAction: TextInputAction.next,
                        attribute: "name",
                        decoration: InputDecoration(
                            labelText: "Full Name",
                            border: UnderlineInputBorder()),
                        onSaved: (name) => contact.name = name,
                        validators: [
                          FormBuilderValidators.required(),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FormBuilderTextField(
                        onSaved: (email) => contact.email = email,
                        textInputAction: TextInputAction.next,
                        attribute: "email",
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            labelText: "Email", border: UnderlineInputBorder()),
                        validators: [
                          FormBuilderValidators.email(),
                          FormBuilderValidators.required(),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FormBuilderTextField(
                        validators: [FormBuilderValidators.required()],
                        onSaved: (phone) => contact.contactNo = phone,
                        textInputAction: TextInputAction.next,
                        attribute: "phone",
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: "Contact No",
                            border: UnderlineInputBorder()),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FormBuilderTextField(
                        onSaved: (msg) => contact.message = msg,
                        textInputAction: TextInputAction.newline,
                        maxLines: 10,
                        minLines: 1,
                        attribute: "message",
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                            labelText: "Message",
                            border: UnderlineInputBorder()),
                        validators: [
                          FormBuilderValidators.required(),
                        ],
                      ),
                    ),
                    MaterialButton(
                      elevation: 5,
                      color: Theme.of(context).primaryColorDark,
                      textColor: Theme.of(context).colorScheme.surface,
                      onPressed: () async {
                        FocusScope.of(context).unfocus();
                        if (_fbKey.currentState.saveAndValidate()) {
                          runMutation(
                            contact.toJson(),
                          );
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: Row(
                            children: [
                              CircularProgressIndicator(),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                "Processing..",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          )));
                          _fbKey.currentState.reset();
                        }
                      },
                      child: Text("SUBMIT"),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
