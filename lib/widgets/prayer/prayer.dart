import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:victoriouslivingkidsclub/data/models/prayer_request_model.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';

class Prayer extends StatefulWidget {
  @override
  _PrayerState createState() => _PrayerState();
}

class _PrayerState extends State<Prayer> {
  List<String> _listPrayerFor;
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>(
      debugLabel: 'prayer-request-form-builder-key');

  PrayerRequest data = PrayerRequest();
  @override
  void initState() {
    super.initState();
    _listPrayerFor = new List<String>();
    _addPrayerFor();
  }

  void _addPrayerFor() {
    _listPrayerFor.add('Prayer For*');
    _listPrayerFor.add('A Neighbour/Co-Worker');
    _listPrayerFor.add('Abuse');
    _listPrayerFor.add('Alchol');
    _listPrayerFor.add('Cancer');
    _listPrayerFor.add('Child Custody');
    _listPrayerFor.add('Church');
    _listPrayerFor.add('Cold & Flu');
    _listPrayerFor.add('Deliverance from Addictions');
    _listPrayerFor.add('Depression');
    _listPrayerFor.add('Diabetes');
    _listPrayerFor.add('Drugs');
    _listPrayerFor.add('Emotional Distress');
    _listPrayerFor.add('Family Member');
    _listPrayerFor.add('Family Situation');
    _listPrayerFor.add('For a Brother/Sister in Christ');
    _listPrayerFor.add('For a Friend');
    _listPrayerFor.add('For a Loved One');
    _listPrayerFor.add('For Finance');
    _listPrayerFor.add('For My Business');
    _listPrayerFor.add('For My Family');
    _listPrayerFor.add('For My Home Church');
    _listPrayerFor.add('For My Job');
    _listPrayerFor.add('For My Ministry');
    _listPrayerFor.add('For Myself');
    _listPrayerFor.add('For Provision');
    _listPrayerFor.add('For Current World Situation');
    _listPrayerFor.add('For the Government');
    _listPrayerFor.add('Heart Problem');
    _listPrayerFor.add('Internal Organs');
    _listPrayerFor.add('Legal Situation');
    _listPrayerFor.add('Life Threates');
    _listPrayerFor.add('Lung Disease');
    _listPrayerFor.add('Marriage Restoration');
    _listPrayerFor.add('Mental Illness');
    _listPrayerFor.add('Military Service');
    _listPrayerFor.add('Others');
    _listPrayerFor.add('Physical Ailment');
    _listPrayerFor.add('Protection');
    _listPrayerFor.add('Rebellious Children');
    _listPrayerFor.add('Salvation');
    _listPrayerFor.add('Sexual Perversion');
    _listPrayerFor.add('Stroke');
    _listPrayerFor.add('Tabacco');
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ChurchAppColors.prayerBackground,
          title: Text('Prayer Request'),
        ),
        body: KeyboardAvoider(
          autoScroll: true,
          curve: Curves.linearToEaseOut,
          child: Builder(builder: (context) {
            return FormBuilder(
              key: _fbKey,
              child: Mutation(
                options: MutationOptions(
                  documentNode: gql(Queries.registerPrayer),
                  onCompleted: (result) {
                    _fbKey.currentState.reset();
                    if (result != null) {
                      print((result as LazyCacheMap).data);
                      final results =
                          (result as LazyCacheMap).data['createPrayer'];
                      if (results != null) {
                        Scaffold.of(context).hideCurrentSnackBar();
                        Scaffold.of(context).showSnackBar(SnackBar(
                            content: Row(
                          children: [
                            Icon(
                              FontAwesomeIcons.checkCircle,
                              color: Colors.green,
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Text(
                              "Uploaded Successfully",
                              style: TextStyle(color: Colors.green),
                            )
                          ],
                        )));
                      }
                    } else {
                      print("null result");
                    }
                  },
                  onError: (error) {
                    _fbKey.currentState.reset();
                    print("error $error");
                    Scaffold.of(context).hideCurrentSnackBar();
                    Scaffold.of(context).showSnackBar(SnackBar(
                        content: Row(
                      children: [
                        Icon(
                          Icons.error_outline,
                          color: Colors.red,
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Text(
                          "An error occured, please try again later..",
                          style: TextStyle(color: Colors.red),
                        )
                      ],
                    )));
                  },
                ),
                builder: (RunMutation runMutation, QueryResult result) {
                  return Container(
                    color: ChurchAppColors.prayerBackground,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12.0, vertical: 12),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Image.asset('assets/prayer.png'),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              Constants.Prayer_TITLE,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: ChurchAppColors.golden,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  letterSpacing: 4),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilderTextField(
                              onSaved: (name) => data.name = name,
                              textInputAction: TextInputAction.next,
                              style: TextStyle(color: Colors.white),
                              attribute: "title",
                              decoration: InputDecoration(
                                labelText: "Full Name",
                                labelStyle:
                                    TextStyle(color: ChurchAppColors.golden),
                              ),
                              validators: [
                                FormBuilderValidators.required(),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilderTextField(
                              onSaved: (email) => data.email = email,
                              textInputAction: TextInputAction.next,
                              attribute: "email",
                              keyboardType: TextInputType.emailAddress,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                labelText: "Email",
                                labelStyle:
                                    TextStyle(color: ChurchAppColors.golden),
                              ),
                              validators: [
                                FormBuilderValidators.email(),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilderTextField(
                              onSaved: (phone) => data.phone = phone,
                              textInputAction: TextInputAction.next,
                              attribute: "phone",
                              keyboardType: TextInputType.number,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                labelText: "Contact No",
                                labelStyle:
                                    TextStyle(color: ChurchAppColors.golden),
                              ),
                              validators: [
                                FormBuilderValidators.numeric(),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilderTextField(
                              onSaved: (country) => data.country = country,
                              attribute: "country",
                              style: TextStyle(color: Colors.white),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                labelText: "Country",
                                labelStyle:
                                    TextStyle(color: ChurchAppColors.golden),
                              ),
                              validators: [
                                FormBuilderValidators.required(),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilderTextField(
                              onSaved: (address) => data.address = address,
                              textInputAction: TextInputAction.newline,
                              attribute: "address",
                              maxLines: 2,
                              minLines: 1,
                              keyboardType: TextInputType.multiline,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                labelText: "Address",
                                labelStyle:
                                    TextStyle(color: ChurchAppColors.golden),
                              ),
                              validators: [
                                FormBuilderValidators.required(),
                                FormBuilderValidators.minLength(5,
                                    errorText:
                                        'Address must be bigger than 5 characters!')
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilderTextField(
                              onSaved: (state) => data.state = state,
                              textInputAction: TextInputAction.next,
                              attribute: "state",
                              keyboardType: TextInputType.text,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                labelText: "State",
                                labelStyle:
                                    TextStyle(color: ChurchAppColors.golden),
                              ),
                              validators: [
                                FormBuilderValidators.required(),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilderTextField(
                              onSaved: (city) => data.city = city,
                              textInputAction: TextInputAction.next,
                              attribute: "city",
                              keyboardType: TextInputType.text,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                labelText: "City",
                                labelStyle:
                                    TextStyle(color: ChurchAppColors.golden),
                              ),
                              validators: [
                                FormBuilderValidators.required(),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilderTextField(
                              onSaved: (prayerFor) =>
                                  data.prayerFor = prayerFor,
                              textInputAction: TextInputAction.next,
                              attribute: "prayer-for",
                              keyboardType: TextInputType.text,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                labelText: "Prayer For",
                                labelStyle:
                                    TextStyle(color: ChurchAppColors.golden),
                              ),
                              validators: [
                                FormBuilderValidators.required(),
                              ],
                            ),
                          ),
//                        Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child: FormBuilderDropdown(
//                            onSaved: (pFor) => data.prayerFor = pFor,
//                            attribute: 'prayer_for',
//                            style: TextStyle(color: ChurchAppColors.golden),
//                            initialValue: 'prayer for',
//                            items: _listPrayerFor.map((String value) {
//                              return new DropdownMenuItem<String>(
//                                value: value,
//                                child: Text(
//                                  value,
//                                  style:
//                                      TextStyle(color: ChurchAppColors.golden),
//                                ),
//                              );
//                            }).toList()
//                              ..add(DropdownMenuItem(
//                                  value: 'prayer for',
//                                  child: Text(
//                                    'Prayer for',
//                                    style: TextStyle(
//                                        color: ChurchAppColors.golden),
//                                  ))),
//                          ),
//                        ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilderTextField(
                              onSaved: (msg) => data.message = msg,
                              textInputAction: TextInputAction.newline,
                              style: TextStyle(color: Colors.white),
                              minLines: 1,
                              maxLines: 10,
                              keyboardType: TextInputType.multiline,
                              attribute: "message",
                              decoration: InputDecoration(
                                labelText: "Message",
                                labelStyle:
                                    TextStyle(color: ChurchAppColors.golden),
                              ),
                              validators: [
                                FormBuilderValidators.required(),
                              ],
                            ),
                          ),
                          MaterialButton(
                            elevation: 5,
                            color: Theme.of(context).primaryColor,
                            textColor: Theme.of(context).colorScheme.surface,
                            onPressed: () async {
                              FocusScope.of(context).unfocus();
                              if (_fbKey.currentState.saveAndValidate()) {
                                runMutation(
                                  data.toJson(),
                                );
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content: Row(
                                  children: [
                                    CircularProgressIndicator(),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Text(
                                      "Processing..",
                                      style: TextStyle(color: Colors.white),
                                    )
                                  ],
                                )));
                                _fbKey.currentState.reset();
                              }
                            },
                            child: Text("SUBMIT"),
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            );
          }),
        ),
      ),
    );
  }
}
