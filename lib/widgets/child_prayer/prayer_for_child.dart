import 'package:flutter/material.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:victoriouslivingkidsclub/data/models/child_prayer_model.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';
import 'package:victoriouslivingkidsclub/widgets/child_prayer/now_playing_screen.dart';

class PrayerForChild extends StatelessWidget {
  final RmxAudioPlayer rmxAudioPlayer;

  const PrayerForChild({Key key, this.rmxAudioPlayer}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: Text("Prayer for Your Child"),
      ),
      body: Query(
          options: QueryOptions(documentNode: gql(Queries.getChildPrayer)),
          builder: (QueryResult result,
              {VoidCallback refetch, FetchMore fetchMore}) {
            if (result.hasException) {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Unable to Load data please try again!"),
                ),
              );
            }

            if (result.loading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            var prayers = ChildPrayer.fromJson(result.data);
            if (prayers.childPrayers.isEmpty) {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("No data to show.."),
                ),
              );
            }
            return GridView.builder(
              itemCount: prayers.childPrayers.length,
              itemBuilder: (context, index) {
                return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Hero(
                      tag: prayers.childPrayers[index].id,
                      child: Material(
                        elevation: 5,
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.bottomLeft,
                                end: Alignment.topRight,
                                stops: [
                                  0.5,
                                  0.1,
                                  0.8,
                                  0.8
                                ],
                                colors: [
                                  Theme.of(context).colorScheme.primary,
                                  Theme.of(context).colorScheme.primaryVariant,
                                  Theme.of(context).colorScheme.secondary,
                                  Theme.of(context)
                                      .colorScheme
                                      .secondaryVariant,
                                ]),
                            borderRadius: BorderRadius.all(Radius.circular(8)),
//                              image: DecorationImage(
//                                  fit: BoxFit.fill,
//                                  image: CachedNetworkImageProvider(
//                                      Constants.assetPath +
//                                          "/" +
//                                          songs[index]
//                                              .image
//                                              .replaceAll(" ", "%20"))),
                          ),
                          child: Center(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.black.withOpacity(0.5),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                              ),
                              width: double.maxFinite,
                              height: double.maxFinite,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Material(
                                    color: Colors.transparent,
                                    child: IconButton(
                                        icon: Icon(
                                          FontAwesomeIcons.play,
                                          size: 30,
                                          color: Colors.white.withOpacity(0.7),
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (_) =>
                                                      NowPlayingScreen(
                                                        rmxAudioPlayer:
                                                            this.rmxAudioPlayer,
                                                        track: prayers
                                                                .childPrayers[
                                                            index],
                                                      )));
                                        }),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0),
                                    child: Text(
                                      prayers.childPrayers[index].title,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ));
              },
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
            );
          }),
    );
  }
}
