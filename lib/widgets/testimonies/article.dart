import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:victoriouslivingkidsclub/data/models/testimony_model.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/network_image.dart';

class Article extends StatelessWidget {
  final Testimonies article;
  DateFormat dateFormat = DateFormat('dd MMM, yyyy');

  Article({Key key, this.article}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          article.title,
          overflow: TextOverflow.fade,
          maxLines: 1,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Stack(
            children: <Widget>[
              Hero(
                tag: this.article.hashCode,
                child: Container(
                    height: 300,
                    width: double.infinity,
                    child: PNetworkImage(
                      '${Constants.ASSET_THUMBNAILS_PATH}/thumbnail_${this.article.image.hash}${this.article.image.ext}',
                      fit: BoxFit.cover,
                    )),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(16.0, 250.0, 16.0, 16.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.0)),
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      article.title,
                      style: Theme.of(context).textTheme.title,
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      "${dateFormat.format(DateTime.parse(article.date))}",
                      style: Theme.of(context).textTheme.caption.copyWith(
                          color: Theme.of(context).colorScheme.primaryVariant),
                    ),
                    SizedBox(height: 10.0),
                    Divider(),
                    SizedBox(
                      height: 10.0,
                    ),
//                    Row(
//                      children: <Widget>[
//                        Icon(Icons.favorite_border),
//                        SizedBox(
//                          width: 5.0,
//                        ),
//                        Text("20.2k"),
//                        SizedBox(
//                          width: 16.0,
//                        ),
//                      ],
//                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(article.description)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
