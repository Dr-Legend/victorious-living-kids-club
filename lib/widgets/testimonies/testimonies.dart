import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:victoriouslivingkidsclub/bloc/testimony/testimony_bloc.dart';
import 'package:victoriouslivingkidsclub/data/models/testimony_model.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';
import 'package:intl/intl.dart';
import 'package:victoriouslivingkidsclub/widgets/testimonies/article.dart';

class TestimoniesScreen extends StatefulWidget {
  @override
  _TestimoniesScreenState createState() => _TestimoniesScreenState();
}

class _TestimoniesScreenState extends State<TestimoniesScreen> {
  DateFormat dateFormat = DateFormat('MMM yyyy');

  String monthString;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TestimonyBloc, TestimonyState>(
        builder: (context, TestimonyState state) {
      if (state is LoadingTestimonyState) {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
      if (state is ErrorTestimonyState) {
        return Center(
          child: Text(
              "Unable to fetch Testimonys please try again later.. ${state.error}"),
        );
      }
      if (state is LoadedTestimonyState) {
        if (monthString == null) {
          monthString = state.dates.testimonies.first.substrDate;
        }
        return Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  height: 50,
                  child: DropdownButton(
                    elevation: 5,
                    icon: Icon(Icons.sort),
                    items: state.dates.testimonies
                        .map((date) => DropdownMenuItem(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(date.substrDate),
                              ),
                              value: date.substrDate,
                            ))
                        .toList(),
                    onChanged: (value) {
                      setState(() {
                        monthString = value;
                      });
                      BlocProvider.of<TestimonyBloc>(context)
                          .add(LoadTestimoniesEvent(monthString: value));
                    },
                    value: monthString,
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: false,
                  itemCount: state.videos.testimonies.length ?? 0,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => Article(
                                      article: state.videos.testimonies[index],
                                    )));
                      },
                      child: BlogCard(
                        blogData: state.videos.testimonies[index],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      }
      return Container(
        child: Center(
          child:
              Text("Servers are under maintainance. Please try again later.."),
        ),
      );
    });
  }
}

class BlogCard extends StatelessWidget {
  final Testimonies blogData;
  final double radius;
  final double carouselSize;

  const BlogCard({Key key, this.blogData, this.radius, this.carouselSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 8,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                elevation: 7,
                borderRadius: BorderRadius.circular(50),
                child: SizedBox(
                    height: 70,
                    width: 70,
                    child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Hero(
                            tag: this.blogData.hashCode,
                            child: CachedNetworkImage(
                              imageUrl:
                                  "${Constants.ASSET_THUMBNAILS_PATH}/thumbnail_${this.blogData.image.hash}${this.blogData.image.ext}",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ))),
              ),
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                    child: Text(
                      this.blogData.title,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(
//                        left: 8.0, bottom: 8.0, right: 8.0),
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: <Widget>[
//                        Text(
//                          DateFormat('dd/MM/yyyy')
//                              .format(DateTime.parse(this.blogData.createdAt)),
//                          style: Theme.of(context).textTheme.caption,
//                        ),
//                        Text(
//                          DateFormat('hh:mm a')
//                              .format(DateTime.parse(this.blogData.createdAt)),
//                          style: Theme.of(context).textTheme.caption,
//                        ),
//                      ],
//                    ),
//                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
