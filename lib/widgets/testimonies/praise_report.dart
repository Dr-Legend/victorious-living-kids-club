import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:victoriouslivingkidsclub/data/models/praise_reports_model.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';

class PraiseReport extends StatelessWidget {
  final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>(debugLabel: 'contact-us-form-builder-key');

  PraiseReportData praiseReportData = PraiseReportData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColorDark,
        title: Text("Praise Report"),
      ),
      body: Builder(builder: (BuildContext context) {
        return KeyboardAvoider(
          autoScroll: true,
          curve: Curves.linearToEaseOut,
          child: FormBuilder(
            key: _fbKey,
            child: Mutation(
              options: MutationOptions(
                documentNode: gql(Queries.registerPraiseReport),
                onCompleted: (result) {
                  if (result != null) {
                    print((result as LazyCacheMap).data);
                    final results =
                        (result as LazyCacheMap).data['createPraiseReport'];
                    if (results != null) {
                      Scaffold.of(context).hideCurrentSnackBar();
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: Row(
                        children: [
                          Icon(
                            FontAwesomeIcons.checkCircle,
                            color: Colors.green,
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            "Uploaded Successfully",
                            style: TextStyle(color: Colors.green),
                          )
                        ],
                      )));
                    }
                  } else {
                    print("null result");
                  }
                },
                onError: (error) {
                  print("error $error");
                  Scaffold.of(context).hideCurrentSnackBar();
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: Row(
                    children: [
                      Icon(
                        Icons.error_outline,
                        color: Colors.red,
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        "An error occured, please try again later..",
                        style: TextStyle(color: Colors.red),
                      )
                    ],
                  )));
                },
              ),
              builder: (RunMutation runMutation, QueryResult result) {
                return Container(
//              color: Color(0xFF495764),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 12.0, vertical: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            Constants.praise_report,
                            style: TextStyle(),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Send Praise Report',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FormBuilderTextField(
                            textInputAction: TextInputAction.next,
                            attribute: "name",
                            decoration: InputDecoration(
                                labelText: "Full Name",
                                border: UnderlineInputBorder()),
                            onSaved: (name) => praiseReportData.name = name,
                            validators: [
                              FormBuilderValidators.required(),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FormBuilderTextField(
                            onSaved: (email) => praiseReportData.email = email,
                            textInputAction: TextInputAction.next,
                            attribute: "email",
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                                labelText: "Email",
                                border: UnderlineInputBorder()),
                            validators: [
                              FormBuilderValidators.email(),
                              FormBuilderValidators.required(),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FormBuilderTextField(
                            validators: [FormBuilderValidators.required()],
                            onSaved: (phone) => praiseReportData.mobile = phone,
                            textInputAction: TextInputAction.next,
                            attribute: "phone",
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: "Contact No",
                                border: UnderlineInputBorder()),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FormBuilderTextField(
                            validators: [FormBuilderValidators.required()],
                            onSaved: (address) =>
                                praiseReportData.address = address,
                            textInputAction: TextInputAction.next,
                            attribute: "address",
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                labelText: "Address",
                                border: UnderlineInputBorder()),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FormBuilderTextField(
                            validators: [FormBuilderValidators.required()],
                            onSaved: (zip) => praiseReportData.zip = zip,
                            textInputAction: TextInputAction.next,
                            attribute: "zip",
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                labelText: "Zip",
                                border: UnderlineInputBorder()),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FormBuilderTextField(
                            onSaved: (testimony) =>
                                praiseReportData.testimony = testimony,
                            textInputAction: TextInputAction.newline,
                            maxLines: 10,
                            minLines: 1,
                            attribute: "testimony",
                            keyboardType: TextInputType.multiline,
                            decoration: InputDecoration(
                                labelText: "Testimony",
                                border: UnderlineInputBorder()),
                            validators: [
                              FormBuilderValidators.required(),
                            ],
                          ),
                        ),
                        MaterialButton(
                          elevation: 5,
                          color: Theme.of(context).primaryColorDark,
                          textColor: Theme.of(context).colorScheme.surface,
                          onPressed: () async {
                            FocusScope.of(context).unfocus();
                            if (_fbKey.currentState.saveAndValidate()) {
                              runMutation(
                                praiseReportData.toJson(),
                              );
                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Row(
                                children: [
                                  CircularProgressIndicator(),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    "Processing..",
                                    style: TextStyle(color: Colors.white),
                                  )
                                ],
                              )));
                              _fbKey.currentState.reset();
                            }
                          },
                          child: Text("SUBMIT"),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      }),
    );
  }
}
