import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:victoriouslivingkidsclub/data/models/child_verse_model.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';

class ChildVerse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Verse For Your Child"),
      ),
      body: Query(
          options: QueryOptions(documentNode: gql(Queries.getChildVerse)),
          builder: (QueryResult result,
              {VoidCallback refetch, FetchMore fetchMore}) {
            if (result.hasException) {
              return Center(
                child: Text("Unable to Load data please try again!"),
              );
            }

            if (result.loading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            var verse = VerseChild.fromJson(result.data);
            return Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Theme.of(context).primaryColorDark,
              child: Center(
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(50),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Theme.of(context).colorScheme.primaryVariant,
                          blurRadius: 250,
                        ),
                        BoxShadow(
                          color: Theme.of(context).colorScheme.primary,
                          blurRadius: 50,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Text(
                        verse.childVerses.isEmpty
                            ? "No Verse Available!"
                            : verse.childVerses.first.description ??
                                "No Verse Available!",
                        style: TextStyle(
                            fontFamily: 'IMFellEnglishSC',
                            fontSize: 21,
                            color: Colors.white),
                      ),
                    )),
              ),
            );
          }),
    );
  }
}
