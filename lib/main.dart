import 'package:fluid_bottom_nav_bar/fluid_bottom_nav_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:flutter_splash_screen/flutter_splash_screen.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/services.dart';
import 'package:victoriouslivingkidsclub/bloc/home/home_bloc.dart';
import 'package:victoriouslivingkidsclub/bloc/testimony/testimony_bloc.dart';
import 'package:victoriouslivingkidsclub/bloc/video/video_bloc.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/firebase_notification.dart';
import 'package:victoriouslivingkidsclub/widgets/contact_us/contact_us.dart';
import 'package:victoriouslivingkidsclub/widgets/home/home.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:victoriouslivingkidsclub/widgets/register_child/register_child.dart';
import 'package:victoriouslivingkidsclub/widgets/testimonies/praise_report.dart';
import 'package:victoriouslivingkidsclub/widgets/testimonies/testimonies.dart';
import 'package:victoriouslivingkidsclub/widgets/way_opener/way_opener.dart';

RmxAudioPlayer rmxAudioPlayer = new RmxAudioPlayer();

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  final HttpLink httpLink = HttpLink(
    uri: Constants.GraphQl_API,
  );

  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      cache: NormalizedInMemoryCache(
        dataIdFromObject: typenameDataIdFromObject,
      ),
      link: httpLink,
    ),
  );

  runApp(
    GraphQLProvider(
      child: CacheProvider(
        child: MyApp(),
      ),
      client: client,
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => HomeBloc()),
        BlocProvider(
          create: (context) => TestimonyBloc(context),
        ),
        BlocProvider(
          create: (context) => VideoBloc(context),
        ),
      ],
      child: MaterialApp(
        title: 'Victorious Living Kids CLub',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Color(0xFF103F4C),
          primaryColorDark: Color(0xFF298490),
          inputDecorationTheme: InputDecorationTheme(
              focusColor: Colors.red,
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFF0f72a8)),
                  borderRadius: BorderRadius.all(Radius.circular(15)))),
          accentColor: Color(0xFFF9C012),
          colorScheme: ColorScheme(
              primary: Color(0xFF103F4C),
              primaryVariant: Color(0xFF298490),
//              primary: Color(0xFFF79C2A),
//              primaryVariant: Color(0xFFD17C18),
              secondary: Color(0xFF69BEF8),
              secondaryVariant: Color(0xFF3D85B7),
//              secondary: Color(0xFFF79C2A),
//              secondaryVariant: Color(0xFFD17C18),
              surface: Color(0xFFFFFFFF),
              background: Color(0xFFFFFFFF),
              error: Color(0xFFB00020),
              onPrimary: Color(0xFFFFFFFF),
              onSecondary: Color(0xFF000000),
              onSurface: Color(0xFF000000),
              onBackground: Color(0xFF000000),
              onError: Color(0xFFFFFFFF),
              brightness: Brightness.light),
//        visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'Victorious Living Kids Club'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Widget _child;
  String title;

  @override
  void initState() {
    title = widget.title;
    _child = Home(
      rmxAudioPlayer: rmxAudioPlayer,
    );
    super.initState();
    rmxAudioPlayer.initialize();
    hideScreen();
  }

  ///hide your splash screen
  Future<void> hideScreen() async {
    Future.delayed(Duration(milliseconds: 3600), () {
      FlutterSplashScreen.hide();
    });
  }

  @override
  void didChangeDependencies() {
    NotificationHandler().initializeFcmNotification(context, rmxAudioPlayer);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (BuildContext context, HomeState state) {
        if (state is ChildChangedState) {
          _handleTitleChange(state.index, context);
        }
      },
      child:
          BlocBuilder<HomeBloc, HomeState>(builder: (context, HomeState state) {
        if (state is ChildChangedState) {
          return Scaffold(
              appBar: AppBar(
                elevation: state.index == 4 ? 0 : 2,
                leading: Image.asset("assets/logo/logo.jpeg"),
                centerTitle: false,
                backgroundColor: Theme.of(context).colorScheme.surface,
                title: Text(
                  title,
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                actions: [
                  state.index == 3
                      ? MaterialButton(
                          onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (_) => PraiseReport())),
                          child: Text("Submit Praise report"),
                        )
                      : Container()
                ],
              ),
              body: SafeArea(
                  child: OfflineBuilder(
                      connectivityBuilder: (
                        BuildContext context,
                        ConnectivityResult connectivity,
                        Widget child,
                      ) {
                        final bool connected =
                            connectivity != ConnectivityResult.none;
                        return Stack(
                          fit: StackFit.expand,
                          children: [
                            child,
                            !connected
                                ? Positioned(
                                    height: 32.0,
                                    left: 0.0,
                                    right: 0.0,
                                    child: AnimatedContainer(
                                      duration:
                                          const Duration(milliseconds: 350),
                                      color: Color(0xFFEE4400),
                                      child: AnimatedSwitcher(
                                        duration:
                                            const Duration(milliseconds: 350),
                                        child: Text('Offline'),
                                      ),
                                    ),
                                  )
                                : Container(),
                          ],
                        );
                      },
                      child: _handleNavigationChange(state.index))),
              bottomNavigationBar: FluidNavBar(
                icons: [
                  FluidNavBarIcon(
                      iconPath: "assets/home.svg",
                      backgroundColor: Color(0xFF4285F4)),
                  FluidNavBarIcon(
                      iconPath: "assets/add_child.svg",
                      backgroundColor: Color(0xFFEC4134)),
                  FluidNavBarIcon(
                      iconPath: "assets/help_child.svg",
                      backgroundColor: Color(0xFFFCBA02)),
                  FluidNavBarIcon(
                      iconPath: "assets/testimony.svg",
                      backgroundColor: Color(0xFF34A950)),
                  FluidNavBarIcon(
                      iconPath: "assets/contact.svg",
                      backgroundColor: Color(0xFF34A950)),
                ],
                onChange: (index) => BlocProvider.of<HomeBloc>(context)
                    .add(ChangeChildEvent(index)),
                style: FluidNavBarStyle(
                  iconUnselectedForegroundColor: Colors.white,
                  barBackgroundColor: Theme.of(context).colorScheme.primary,
                  iconBackgroundColor: Colors.green,
                ),
                scaleFactor: 1.5,
              ));
        }
      }),
    );
  }

  Widget _handleNavigationChange(int index) {
    switch (index) {
      case 0:
        title = widget.title;

        _child = Home(
          rmxAudioPlayer: rmxAudioPlayer,
        );
        break;
      case 1:
        title = "Register A Child";

        _child = RegisterChild();

        break;
      case 2:
        title = "BE A WAY OPENER";

        _child = WayOpener();

        break;
      case 3:
        title = "Testimonies";

        _child = TestimoniesScreen();
        break;
      case 4:
        title = "Contact Us";

        _child = ContactUs();
        break;
    }
    return _child = AnimatedSwitcher(
      switchInCurve: Curves.easeOut,
      switchOutCurve: Curves.easeIn,
      duration: Duration(milliseconds: 500),
      child: _child,
    );
  }

  void _handleTitleChange(int index, BuildContext context) {
    switch (index) {
      case 0:
        setState(() {
          title = widget.title;
        });
        break;
      case 1:
        setState(() {
          title = "Register A Child";
        });

        break;
      case 2:
        setState(() {
          title = "BE A WAY OPENER";
        });

        break;
      case 3:
        BlocProvider.of<TestimonyBloc>(context).add(LoadTestimoniesEvent());
        setState(() {
          title = "Testimonies";
        });
        break;
      case 4:
        setState(() {
          title = "Contact Us";
        });
        break;
    }
  }
}
