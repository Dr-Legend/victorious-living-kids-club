import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

///This class has API calling methods
///
class ApiMethods {
  ///This method handles request in GET
  Future<String> requestInGet(String url) async {
    print("URL is $url in get");
    final response = await http.get(url,
        headers: {"Accept": "application/json"}).timeout(Duration(seconds: 10));
    print(response.body);
    if (response.statusCode == 200 && response.reasonPhrase != "Not Found") {
      /// If server returns an OK response, return the response
      return response.body;
    } else if (response.reasonPhrase == "Not Found") {
      print('Error in response: ${response.reasonPhrase}');
      return null;
      throw Exception('Failed to load data');
    } else {
      print('Error in response: ${response.reasonPhrase}');
      throw Exception('Failed to load data');
    }
  }

  Future<http.Response> requestInPost(
      String url, String tokens, Map<String, dynamic> body) async {
    print("URL is $url in post");

//    final response = await http.post(url,
//        headers: {"Content-Type": "application/json"}, ;
    var client = new http.Client();
    try {
      http.Response uriResponse = await client.post(url,
          headers: {"content-type": "application/json"},
          body: json.encode(body));
      print(url);
      print(body);
      print(uriResponse.body);
      if (uriResponse.statusCode == 200) {
        /// If server returns an OK response, return the response
        return uriResponse;
      } else if (uriResponse.reasonPhrase == "Not Found") {
        print('Error in response: ${uriResponse.reasonPhrase}');
        return null;
        throw Exception('Failed to load data');
      } else {
        print('Error in response: ${uriResponse.reasonPhrase}');
        throw Exception('Failed to load data');
      }
    } finally {
      client.close();
    }
  }

  Future<http.Response> requestInPut(
      String url, String tokens, Map<String, dynamic> body) async {
    print("URL is $url in put");
//    final response = await http.post(url,
//        headers: {"Content-Type": "application/json"}, ;
    var client = new http.Client();
    try {
      http.Response uriResponse = await client.put(url,
          headers: {"content-type": "application/json"},
          body: json.encode(body));
      print(url);
      print(body);
      print(uriResponse.body);
      if (uriResponse.statusCode == 200) {
        /// If server returns an OK response, return the response
        return uriResponse;
      } else if (uriResponse.reasonPhrase == "Not Found") {
        print('Error in response: ${uriResponse.reasonPhrase}');
        return null;
        throw Exception('Failed to load data');
      } else {
        print('Error in response: ${uriResponse.reasonPhrase}');
        throw Exception('Failed to load data');
      }
    } finally {
      client.close();
    }
  }
}
