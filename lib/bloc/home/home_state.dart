part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class InitialHomeState extends HomeState {}

class ChildChangedState extends HomeState {
  final int index;

  ChildChangedState({this.index = 0});
}
