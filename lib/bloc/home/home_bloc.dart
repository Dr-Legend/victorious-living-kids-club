import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  @override
  HomeState get initialState => ChildChangedState(index: 0);

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is ChangeChildEvent) {
      print(event.index);
      yield ChildChangedState(index: event.index);
    }
  }
}
