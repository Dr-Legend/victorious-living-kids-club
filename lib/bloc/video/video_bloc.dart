import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:meta/meta.dart';
import 'package:victoriouslivingkidsclub/data/models/video_archive_model.dart';
import 'package:victoriouslivingkidsclub/data/models/video_model.dart';
import 'package:victoriouslivingkidsclub/shared/constants.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';

part 'video_event.dart';

part 'video_state.dart';

class VideoBloc extends Bloc<VideoEvent, VideoState> {
  final BuildContext context;

  VideoBloc(this.context);
  @override
  VideoState get initialState => InitialVideoState();

  @override
  Stream<VideoState> mapEventToState(VideoEvent event) async* {
    var client = GraphQLProvider.of(context).value;
    if (event is LoadVideosEvent) {
      try {
        var response;
        yield LoadingVideoState();
        var dateRespose = await client
            .query(QueryOptions(documentNode: gql(Queries.getVideoArchives)));
        VideoArchiveDates dates = VideoArchiveDates.fromJson(dateRespose.data);
        if (event.monthString == null)
          response = await client.query(QueryOptions(
              documentNode: gql(Queries.getVideos),
              variables: {"substr_date": dates.videos.first.substrDate}));
        if (event.monthString != null)
          response = await client.query(QueryOptions(
              documentNode: gql(Queries.getVideos),
              variables: {"substr_date": event.monthString}));
        Video videos = Video.fromJson(response.data);

        yield LoadedVideoState(videos: videos, dates: dates);
      } on OperationException catch (e) {
        yield ErrorVideoState(e);
        print(e);
      } catch (e) {
        yield ErrorVideoState(e);
        print(e);
      }
    }
  }
}
