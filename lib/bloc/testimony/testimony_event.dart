part of 'testimony_bloc.dart';

@immutable
abstract class TestimonyEvent {}

class LoadTestimoniesEvent extends TestimonyEvent {
  final String monthString;

  LoadTestimoniesEvent({this.monthString});
}
