import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:meta/meta.dart';
import 'package:victoriouslivingkidsclub/data/models/testimony_archives_model.dart';
import 'package:victoriouslivingkidsclub/data/models/testimony_model.dart';
import 'package:victoriouslivingkidsclub/shared/queries.dart';
import 'package:async/async.dart';
import 'dart:convert';
part 'testimony_event.dart';

part 'testimony_state.dart';

class TestimonyBloc extends Bloc<TestimonyEvent, TestimonyState> {
  final BuildContext context;

  TestimonyBloc(this.context);
  @override
  TestimonyState get initialState => InitialTestimonyState();

  @override
  Stream<TestimonyState> mapEventToState(TestimonyEvent event) async* {
    var client = GraphQLProvider.of(context).value;
    if (event is LoadTestimoniesEvent) {
      try {
        var response;
        yield LoadingTestimonyState();
        var dateRespose = await client.query(QueryOptions(
          documentNode: gql(Queries.getTestimoniesArchives),
        ));

        TestimonyArchives dates = TestimonyArchives.fromJson(dateRespose.data);
        print(dates.testimonies);
        if (event.monthString == null)
          response = await client.query(QueryOptions(
              documentNode: gql(Queries.getTestimonies),
              variables: {"substr_date": dates.testimonies.first.substrDate}));
        if (event.monthString != null)
          response = await client.query(QueryOptions(
              documentNode: gql(Queries.getTestimonies),
              variables: {"substr_date": event.monthString}));
        Testimony videos = Testimony.fromJson(response.data);

        yield LoadedTestimonyState(videos: videos, dates: dates);
      } on OperationException catch (e) {
        yield ErrorTestimonyState(e);
        print(e);
      } catch (e) {
        yield ErrorTestimonyState(e);
        print(e);
      }
    }
  }
}
