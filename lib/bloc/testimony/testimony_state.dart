part of 'testimony_bloc.dart';

@immutable
abstract class TestimonyState {}

class InitialTestimonyState extends TestimonyState {}

@immutable
class ErrorTestimonyState extends TestimonyState {
  final dynamic error;

  ErrorTestimonyState(this.error);
}

@immutable
class LoadedTestimonyState extends TestimonyState {
  final Testimony videos;
  final TestimonyArchives dates;

  LoadedTestimonyState({this.videos, @required this.dates});
}

@immutable
class LoadingTestimonyState extends TestimonyState {}
