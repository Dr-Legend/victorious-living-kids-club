package com.christianappdevelopers.victoriouslivingkidsclub

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import io.flutter.embedding.android.FlutterActivity
import org.devio.flutter.splashscreen.SplashScreen
class MainActivity: FlutterActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        SplashScreen.show(this, true)
        super.onCreate(savedInstanceState)
    }
}
